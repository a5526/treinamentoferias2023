package com.jpaAula;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class JpaAulaApplication {

    public static void main(String[] args) {
        SpringApplication.run(JpaAulaApplication.class, args);
    }

}
